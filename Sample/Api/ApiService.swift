//
//  ApiService.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa
import Alamofire

class ApiService: NSObject {
    
    var provider: MoyaProvider<SampleAPI>!
    let disposeBag = DisposeBag()
    
    override init() {
        self.provider = MoyaProvider<SampleAPI>(endpointClosure: endpointClosure, requestClosure: requestClosure,  plugins: [NetworkLoggerPlugin(verbose: true)])
    }

}
