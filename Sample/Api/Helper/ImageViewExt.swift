//
//  ImageViewExt.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func imageFromURL(imageURL : String){
        
        if  SDImageCache.shared().diskImageDataExists(withKey: URL(string: imageURL)?.absoluteString) {
            self.image = SDImageCache.shared().imageFromDiskCache(forKey: URL(string: imageURL)?.absoluteString)
        }else{
            self.sd_setImage(
                with: URL(string: imageURL),
                placeholderImage: nil,
                options:[.refreshCached,.cacheMemoryOnly]) { (image, error, cacheType, url) in
                    
                    DispatchQueue.main.async { () -> Void in
                        
                        if error == nil {
                            SDImageCache.shared().store(image, forKey: URL(string: imageURL)?.absoluteString, completion: {
                                self.image = image
                            })
                        } else {
                            self.image = UIImage(named: "placeholder")
                        }
                    }
            }
        }
    }
}
