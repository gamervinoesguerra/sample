
//
//  SampleItemModel.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit

class SampleItemModel: Codable {

    var title : String?
    var description : String?
    var imageHref : String?
    
    enum SampleItemModelCodingKeys : String, CodingKey {
        case title
        case description
        case imageHref
    }
}
