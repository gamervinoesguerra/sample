//
//  SampleApiProvider.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Moya

class SampleApiProvider: ApiService {
    
    
    override init() {
        super.init()
    }
    
    
    func getData()->Observable<Response>{
        return self.provider.rx.request(.getData()).asObservable()
    }
}
