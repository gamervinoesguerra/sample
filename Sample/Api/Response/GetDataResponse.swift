//
//  GetDataResponse.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit

class GetDataResponse: Codable {
    var title : String?
    var rows : [SampleItemModel]?
    var message : String?
    var status : Int? = 0
    
    init(message:String,status:Int) {
        self.status = status
        self.message = message
    }
    
    enum GetDataResponseCodingKeys : String, CodingKey {
        case title
        case rows
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: GetDataResponseCodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        rows = try container.decode([SampleItemModel].self, forKey: .rows)
    }
}
