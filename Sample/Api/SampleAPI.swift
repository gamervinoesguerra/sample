//
//  SampleAPI.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import Moya
import Alamofire

enum SampleAPI {
    case getData()
}

extension SampleAPI: TargetType {
    
    var baseURL: URL { return URL(string: API.BASEURL)! }
    
    var path: String {
        switch self {
        case .getData:
            return API.DATAURL
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var method: Moya.Method {
        switch self {
        case .getData:
             return .get
        }
    }
    
    var task: Task {
        switch self {
        case .getData:
            return .requestPlain
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var sampleData: Data {
        switch self {
        case .getData:
            return "".data(using: .utf8)!
        }
    }
}


public func url(route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

let endpointClosure = { (target: SampleAPI) -> Endpoint in
    let defaultEndpoint = MoyaProvider<SampleAPI>.defaultEndpointMapping(for: target)
    switch target{
        case .getData:
        return defaultEndpoint.adding(newHTTPHeaderFields: ["Content-Type" : "text/plain; charset=ISO-8859-1"])
    }
}

let requestClosure = { (endpoint: Endpoint, done: @escaping MoyaProvider.RequestResultClosure) in
    var request = try! endpoint.urlRequest() as URLRequest
    done(.success(request))
}

private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

