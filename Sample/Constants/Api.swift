//
//  Api.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit

struct API {
    static let BASEURL = "https://dl.dropboxusercontent.com"
    static let DATAURL = "/s/2iodh4vg0eortkl/facts.json"
}
