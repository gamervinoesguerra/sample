//
//  SampleTableviewCellTableViewCell.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import SDWebImage

class SampleTableviewCellTableViewCell: UITableViewCell {
    
    lazy var titleLbl = UILabel()
    lazy var descriptionLbl = UILabel()
    lazy var iconImgVIew = UIImageView()
    
    var isLoaded = false
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.iconImgVIew)
        self.contentView.addSubview(self.titleLbl)
        self.contentView.addSubview(self.descriptionLbl)
      
        self.titleLbl.numberOfLines = 0
        self.descriptionLbl.numberOfLines = 0
        
        self.iconImgVIew.image = UIImage(named: "placeholder")
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //Setup Constraints
    private func setupView(){
        
        self.iconImgVIew.backgroundColor = UIColor.black
        self.titleLbl.backgroundColor = UIColor.green
        self.descriptionLbl.backgroundColor = UIColor.red
        
        self.iconImgVIew.snp.makeConstraints { (make) in
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.top.equalTo(self.contentView).offset(16)
            make.left.equalTo(self.contentView).offset(16)
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-16)
        }
        
        self.titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(16)
            make.left.equalTo(self.iconImgVIew.snp.right).offset(8)
            make.right.equalTo(self.contentView).offset(-16)
            make.bottom.equalTo(self.descriptionLbl.snp.top).offset(-8)
        }
        
        self.descriptionLbl.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLbl.snp.bottom).offset(-8)
            make.left.equalTo(self.iconImgVIew.snp.right).offset(8)
            make.right.equalTo(self.contentView).offset(-16)
            make.bottom.equalTo(self.contentView).offset(-16)
        }

    }
    
    func loadItem(item:SampleItemModel){
        self.titleLbl.text = item.title
        self.descriptionLbl.text = item.description
        
        if item.imageHref != nil{
            self.iconImgVIew.imageFromURL(imageURL: item.imageHref!)
        }else{
            self.iconImgVIew.image = UIImage(named: "placeholder")
        }
    }

}
