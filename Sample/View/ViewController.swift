//
//  ViewController.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import CRRefresh

class ViewController: UIViewController {
    
    var viewModel : SampleViewModel!
    
    var disposeBag = DisposeBag()
    
    lazy var contentTblView = UITableView()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupView()
        setupRx()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //setup view
    func setupView(){
        
        self.contentTblView.rowHeight = UITableViewAutomaticDimension
        self.contentTblView.estimatedRowHeight = 250
        
        self.contentTblView.allowsSelection = false
        
        self.contentTblView.delegate = self
        self.contentTblView.dataSource = self
        self.view.addSubview(contentTblView)
        
        self.contentTblView.register(SampleTableviewCellTableViewCell.self, forCellReuseIdentifier: "SampleTableviewCellTableViewCellID")
        
        self.contentTblView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
        
        self.contentTblView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            self?.viewModel.getData()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext : { isSuccess in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        self?.contentTblView.reloadData()
                        self?.contentTblView.cr.endHeaderRefresh()
                    })
                }).disposed(by: (self?.disposeBag)!)
        }
    }
    
    
    //Setup RX Bindings
    func setupRx(){
        
        self.viewModel = SampleViewModel(inVC: self)
        
        self.viewModel.pageTitle.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe({ title in
                self.navigationItem.title = title.element
            }).disposed(by: self.disposeBag)
        
        
        self.viewModel.data.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe({ _ in
                self.contentTblView.reloadData()
            }).disposed(by: self.disposeBag)
        
        self.viewModel.getData().asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe({ _ in
                self.contentTblView.reloadData()
            }).disposed(by: self.disposeBag)
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.viewModel.data.value?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SampleTableviewCellTableViewCellID") as! SampleTableviewCellTableViewCell
        cell.loadItem(item: self.viewModel.data.value![indexPath.row])
        

        return cell
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
