//
//  SampleViewModel.swift
//  Sample
//
//  Created by Olivin Esguerra on 30/04/2018.
//  Copyright © 2018 Olivin Esguerra. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya

class SampleViewModel {
    
    var provider = SampleApiProvider()
    var data : Variable<[SampleItemModel]?> = Variable([])
    var inVC : UIViewController!
    var pageTitle:Variable<String> = Variable("")
    
    init(inVC : UIViewController){
        self.inVC = inVC
    }
    
    func getData()->Observable<Bool>{
        return self.provider.getData()
            .observeOn(MainScheduler.instance)
            .catchError({ (error) -> Observable<Response> in
                return Observable.just(Response(statusCode: -1, data: error.localizedDescription.data(using: String.Encoding.utf8)!))
            }).flatMapLatest({ (response) -> Observable<Bool> in
                print(String(data: response.data, encoding: String.Encoding.ascii)!)
                
                if response.statusCode != 1 {
                   
                    do {
                        if let jsonData = String(data: response.data, encoding: String.Encoding.ascii)!.data(using: .utf8) {
                            let jsonObj = try JSONDecoder().decode(GetDataResponse.self, from: jsonData)
                            print(jsonObj)
                            self.data.value = jsonObj.rows
                            self.pageTitle.value = jsonObj.title!

                            return Observable.just(true)
                        }else{
                            return AlertDialog.shared.promptFor("Error", "Bad JSON", cancelAction: "OK", actions: [], vc: self.inVC)
                                .map {_ in
                                    return false
                            }

                        }
                    } catch let error as NSError {
                        return AlertDialog.shared.promptFor("Error", error.localizedDescription, cancelAction: "OK", actions: [], vc: self.inVC)
                            .map {_ in
                                return false
                        }
                    }
                }else{
                    return AlertDialog.shared.promptFor("Error", String(data: response.data, encoding: .utf8)!, cancelAction: "OK", actions: [], vc: self.inVC)
                        .map {_ in
                            return false
                    }
                }
            })
    }
}
